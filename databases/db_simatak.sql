/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.32-MariaDB : Database - db_simatak
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_simatak` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_simatak`;

/*Table structure for table `t_brg_keluar` */

DROP TABLE IF EXISTS `t_brg_keluar`;

CREATE TABLE `t_brg_keluar` (
  `id_brg_keluar` int(3) NOT NULL AUTO_INCREMENT,
  `kd_item` varchar(20) NOT NULL,
  `nama_item` varchar(20) DEFAULT NULL,
  `jml_keluar` int(3) DEFAULT NULL,
  `nama_pengambil` varchar(25) DEFAULT NULL,
  `tanggal_brg_keluar` date DEFAULT NULL,
  PRIMARY KEY (`kd_item`),
  KEY `id_brg_keluar` (`id_brg_keluar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_brg_keluar` */

/*Table structure for table `t_brg_masuk` */

DROP TABLE IF EXISTS `t_brg_masuk`;

CREATE TABLE `t_brg_masuk` (
  `id_brg_masuk` varchar(20) NOT NULL,
  `kd_item` varchar(20) DEFAULT NULL,
  `jumlah_brg_masuk` int(3) DEFAULT NULL,
  `tanggal_brg_masuk` date DEFAULT NULL,
  PRIMARY KEY (`id_brg_masuk`),
  KEY `id_brg` (`id_brg_masuk`),
  KEY `kd_item` (`kd_item`),
  CONSTRAINT `t_brg_masuk_ibfk_2` FOREIGN KEY (`kd_item`) REFERENCES `t_master_brg` (`kd_item`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_brg_masuk` */

insert  into `t_brg_masuk`(`id_brg_masuk`,`kd_item`,`jumlah_brg_masuk`,`tanggal_brg_masuk`) values 
('IN0001','BRG-0001',10,'2018-08-16');

/*Table structure for table `t_master_brg` */

DROP TABLE IF EXISTS `t_master_brg`;

CREATE TABLE `t_master_brg` (
  `kd_item` varchar(20) NOT NULL,
  `nama_item` varchar(25) DEFAULT NULL,
  `satuan` varchar(15) DEFAULT NULL,
  `harga_satuan` double DEFAULT NULL,
  PRIMARY KEY (`kd_item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_master_brg` */

insert  into `t_master_brg`(`kd_item`,`nama_item`,`satuan`,`harga_satuan`) values 
('BRG-0001','Kertas F4','Rim',8500),
('BRG-0002','Micro SD Sand Disk','Pcs',80000),
('BRG-0003','Micro SD V-Gen','Pcs',70000),
('BRG-0004','Pulpen Snowman','Dus',45000),
('BRG-0005','Kertas A4','Rim',10005);

/*Table structure for table `t_penganggaran` */

DROP TABLE IF EXISTS `t_penganggaran`;

CREATE TABLE `t_penganggaran` (
  `id_anggaran` int(3) NOT NULL AUTO_INCREMENT,
  `kd_item` varchar(20) DEFAULT NULL,
  `nama_item` varchar(20) DEFAULT NULL,
  `triwulan` varchar(15) DEFAULT NULL,
  `jumlah` int(3) DEFAULT NULL,
  `sub_total` double DEFAULT NULL,
  `tanggal_anggaran` date DEFAULT NULL,
  PRIMARY KEY (`id_anggaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_penganggaran` */

/*Table structure for table `t_rincian_brg` */

DROP TABLE IF EXISTS `t_rincian_brg`;

CREATE TABLE `t_rincian_brg` (
  `id_rincian` int(3) NOT NULL AUTO_INCREMENT,
  `kd_item_masuk` varchar(20) DEFAULT NULL,
  `kd_item_keluar` varchar(20) DEFAULT NULL,
  KEY `id_rincian` (`id_rincian`),
  KEY `kd_item_masuk` (`kd_item_masuk`),
  KEY `kd_item_keluar` (`kd_item_keluar`),
  CONSTRAINT `t_rincian_brg_ibfk_1` FOREIGN KEY (`kd_item_masuk`) REFERENCES `t_brg_masuk` (`kd_item`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_rincian_brg_ibfk_2` FOREIGN KEY (`kd_item_keluar`) REFERENCES `t_brg_keluar` (`kd_item`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_rincian_brg` */

/*Table structure for table `t_sisa_brg` */

DROP TABLE IF EXISTS `t_sisa_brg`;

CREATE TABLE `t_sisa_brg` (
  `id_sisa` int(3) NOT NULL AUTO_INCREMENT,
  `kd_item` varchar(20) DEFAULT NULL,
  `nama_item` varchar(20) DEFAULT NULL,
  `jml_sisa` int(3) DEFAULT NULL,
  PRIMARY KEY (`id_sisa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_sisa_brg` */

/*Table structure for table `t_users` */

DROP TABLE IF EXISTS `t_users`;

CREATE TABLE `t_users` (
  `nip` varchar(40) NOT NULL,
  `nama` varchar(35) DEFAULT NULL,
  `jabatan` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `passwords` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_users` */

insert  into `t_users`(`nip`,`nama`,`jabatan`,`email`,`passwords`) values 
('197207201992022003','Ira Eka Riani','kasi','','admin456'),
('197304041992031001','Drs. Hasanudin, M.Si','kabid','','admin23456'),
('198303302009011007','Rochmat Bachtiar S.Ap','kasi','','admin12345'),
('kemem','kemem','VUVATI','oki.prasetyo45@gmail.com','admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
