<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barangsisa extends CI_Controller {

	public function index()
	{
		$this->load->view('design/header');
		$this->load->view('design/sidebar');
		$this->load->view('master/v_barangsisa');
		$this->load->view('design/rightsidebar');
		$this->load->view('design/footer');
	}

}

/* End of file Barangsisa.php */
/* Location: ./application/controllers/Barangsisa.php */