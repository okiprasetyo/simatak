<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->model('m_login');
	}

	public function index()
	{
		$cek = $this->session->userdata('role');

		if (empty($cek)) {
				$this->load->view('auth/v_login');
				//var_dump($cek); exit();
			}
		else{
			header('location:'.base_url().'dashboard');
		}	
		
	}

	public function validate(){
		$cek = $this->session->userdata('role');
		if (empty($cek)) {
			$this->form_validation->set_rules('nip', 'nip', 'required');
			$this->form_validation->set_rules('passwords', 'passwords', 'required');
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('v_login');
			} else {
				$nip = $this->input->post('nip');
				$p = $this->input->post('passwords');
				$this->m_login->getLoginData($nip, $psw);
			}
		}
		else{
			header('location:'.base_url().'login');
		}
	}

	 public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
		

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */