<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggaran_tw2 extends CI_Controller {

	public function index()
	{
		$this->load->view('design/header');
		$this->load->view('design/sidebar');
		$this->load->view('master/v_anggaran_tw2');
		$this->load->view('design/rightsidebar');
		$this->load->view('design/footer');
	}

}

/* End of file Anggaran_tw2.php */
/* Location: ./application/controllers/Anggaran_tw2.php */