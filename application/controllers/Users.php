<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('encryption');
		$this->load->model('m_users');
	}

	public function index()
	{
		$this->load->view('design/header');
		$this->load->view('design/sidebar');
		$this->load->view('master/v_user');
		$this->load->view('design/rightsidebar');
		$this->load->view('design/footer');
	}

	public function get_json() {
        $list = $this->m_users->get_datatables();
        $data = array();
        $no = 0;
        foreach ($list as $result) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $result->nip;
            $row[] = $result->nama;
            $row[] = $result->jabatan;
            $row[] = $result->email;
            $row[] = $result->passwords;
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit('."'".$result->nip."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                      <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_user('."'".$result->nip."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            $data[] = $row;
        }
        header('Content-type: application/json');
        echo json_encode(array('data' => $data));
    }

    public function ajax_kode(){
    	$data['kode_user'] = $this->m_users->getMaxKodeUser();
    	echo json_encode($data);
    }

    public function ajax_add(){
    	$this->_validate();
    	$data = array(
    				'nip' => $this->input->post('nip'),
    				'nama' => $this->input->post('nama'),
    				'email' => $this->input->post('email'),
    				'jabatan' => $this->input->post('jabatan'),
    				'passwords' => $this->input->post('passwords'),
    				);
    	$insert = $this->m_users->save($data);
    	echo json_encode(array("status" => TRUE));
    }

    public function ajax_edit($id){
    	$data = $this->m_users->get_by_id($id);
    	echo json_encode($data);
    }

    public function ajax_update(){
    	$this->_validate();
    	$data = array(
    				'nip' => $this->input->post('nip'),
    				'nama' => $this->input->post('nama'),
    				'email' => $this->input->post('email'),
    				'jabatan' => $this->input->post('jabatan'),
    				'passwords' => $this->input->post('passwords'),
    				);
    	$this->m_users->update(array('nip'=>$this->input->post('nip')), $data);
    	echo json_encode(array("status" => TRUE));
    }

    private function _validate(){
    	$data = array();
    	$data['error_string'] = array();
    	$data['inputerror'] = array();
    	$data['status'] = TRUE;

    	if($this->input->post('nip') == ''){
    		$data['inputerror'][] = 'nip';
    		$data['error_string'][] = 'NIP harus di isi !';
    		$data['status'][] = FALSE;
    	}

    	if($this->input->post('nama') == '')
        {
            $data['inputerror'][] = 'nama';
            $data['error_string'][] = 'Nama Harus Di isi!!';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }

    public function ajax_delete($id){
    	$this->m_users->delete_by_id($id);
    	echo json_encode(array("status" => TRUE));
    }
}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */