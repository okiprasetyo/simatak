<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	 public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('m_users');
    }  
    

	public function index()
	{
		$cek = $this->session->userdata('role');
        if(empty($cek))
        {
            $this->load->view("auth/v_login");
            
        }else{
            header('location:'.base_url().'dashboard');

        }
	}

	 public function validate() {
        $cek = $this->session->userdata('role');
        if(empty($cek))
        {
        $this->form_validation->set_rules('nip', 'nip', 'required');
        $this->form_validation->set_rules('passwords', 'psswords', 'required');
        if ($this->form_validation->run() == FALSE){
                $this->load->view("auth/v_login");
            }
            else{
                $u = $this->input->post('nip');
                $p = $this->input->post('passwords');
                $this->manajemen_user->getLoginData($u,$p);
            }
        }else{
            header('location:'.base_url().'auth');
        }
    }
    

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('auth');
    }

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */