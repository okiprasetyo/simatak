<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barangkeluar extends CI_Controller {

	public function index()
	{
		$this->load->view('design/header');
		$this->load->view('design/sidebar');
		$this->load->view('master/v_barangkeluar');
		$this->load->view('design/rightsidebar');
		$this->load->view('design/footer');
	}

}

/* End of file Barangkeluar.php */
/* Location: ./application/controllers/Barangkeluar.php */