<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->library('encryption');
        $this->load->model('m_barang');
	}

	public function index()
	{
		$this->load->view('design/header');
		$this->load->view('design/sidebar');
		$this->load->view('master/v_barang');
		$this->load->view('design/rightsidebar');
		$this->load->view('design/footer');
	}

	 public function get_json() {
        $list = $this->m_barang->get_datatables();
        $data = array();
        $no = 0;
        foreach ($list as $result) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $result->kd_item;
            $row[] = $result->nama_item;
            $row[] = $result->satuan;
            $row[] = $result->harga_satuan;
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit('."'".$result->kd_item."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                      <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_barang('."'".$result->kd_item."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            $data[] = $row;
        }
        header('Content-type: application/json');
        echo json_encode(array('data' => $data));
    }

    public function ajax_kode(){
        $data['kode_item'] = $this->m_barang->getMaxKodeBarang();
        echo json_encode($data);
    }

     public function ajax_add(){
        $this->_validate();
        $data = array(
                'kd_item' => $this->input->post('kd_item'),
                'nama_item' => $this->input->post('nama_item'),
                'satuan' => $this->input->post('satuan'),
                'harga_satuan' => $this->input->post('harga_satuan'),
            );
        $insert = $this->m_barang->save($data);
        echo json_encode(array("status" => TRUE));
    }
    
    public function ajax_edit($id){
        $data = $this->m_barang->get_by_id($id);
        echo json_encode($data);
    }

     public function ajax_update(){
        $this->_validate();
        $data = array(
                'kd_item' => $this->input->post('kd_item'),
                'nama_item' => $this->input->post('nama_item'),
                'satuan' => $this->input->post('satuan'),
                'harga_satuan' => $this->input->post('harga_satuan'),
            );
        $this->m_barang->update(array('kd_item' => $this->input->post('kd_item')), $data);
        echo json_encode(array("status" => TRUE));
    }

     private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('kd_item') == ''){
            $data['inputerror'][] = 'kd_item';
            $data['error_string'][] = 'Kode Barang/ Item Harus Di isi!!';
            $data['status'] = FALSE;
        }
		if($this->input->post('nama_item') == ''){
            $data['inputerror'][] = 'nama_item';
            $data['error_string'][] = 'Nama Barang Harus Di isi!!';
            $data['status'] = FALSE;
      	}
		if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

     public function ajax_delete($id){
        $this->m_barang->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
}

/* End of file Barang.php */
/* Location: ./application/controllers/Barang.php */