<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barangmasuk extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('encryption');
		$this->load->model('m_barang_masuk');
	}

	public function index()
	{
		
		$this->load->view('design/header');
		$this->load->view('design/sidebar');
		$this->load->view('master/v_barang_masuk');
		$this->load->view('design/rightsidebar');
		$this->load->view('design/footer');
	}

	public function ajax_kode(){
		$data['kd_brg_masuk'] = $this->m_barang_masuk->getMaxKodeBrgMasuk();
		$data['kd_brg'] = $this->m_barang_masuk->get_kd_brg();
        echo json_encode($data);
		
	}

	public function get_kd_item(){
		$list = $this->m_barang_masuk->get_kd_brg();
		$data = array();
		foreach ($list as $result) {
			$row = array();
			$row[] = $row->kd_item;
			$data = $row;
		}
		header('Content-type: application/json');
        echo json_encode(array('data' => $data));
	}

	public function get_json() {
        $list = $this->m_barang_masuk->get_barang_masuk();

        $data = array();
        $no = 0;
        foreach ($list as $result) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $result->id_brg_masuk;
            $row[] = $result->kd_item;
            $row[] = $result->nama_item;
            $row[] = $result->jumlah_brg_masuk;
            $row[] = $result->tanggal_brg_masuk;
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit('."'".$result->id_brg_masuk."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                      <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_barang('."'".$result->id_brg_masuk."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            $data[] = $row;
        }
        header('Content-type: application/json');
        echo json_encode(array('data' => $data));
    }

    public function ajax_edit($id){
        $data = $this->m_barang_masuk->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_update(){
        $this->_validate();
        $data = array(
                'id_brg_masuk' => $this->input->post('id_brg_masuk'),
                'kd_item' => $this->input->post('kd_item'),
                'jumlah_brg_masuk' => $this->input->post('jumlah_brg_masuk'),
                'tanggal_brg_masuk' => $this->input->post('tanggal_brg_masuk'),
            );
        $this->m_barang_masuk->update(array('id_brg_masuk' => $this->input->post('id_brg_masuk')), $data);
        echo json_encode(array("status" => TRUE));
    }

     private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('kd_item') == ''){
            $data['inputerror'][] = 'kd_item';
            $data['error_string'][] = 'Kode Barang/ Item Harus Di isi!!';
            $data['status'] = FALSE;
        }
		if($this->input->post('jumlah_brg_masuk') == ''){
            $data['inputerror'][] = 'jumlah_brg_masuk';
            $data['error_string'][] = 'Jumlah Barang Harus Di isi!!';
            $data['status'] = FALSE;
      	}
		if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
     }
}

/* End of file Barangmasuk.php */
/* Location: ./application/controllers/Barangmasuk.php */