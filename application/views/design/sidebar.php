<!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="<?php echo base_url("assetos/theme/assets/images/users/avatar-1.jpg") ;?>" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                            <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
                        </div>
                        <h5><a href="#">Vicki Burket</a> </h5>
                        <ul class="list-inline">
                            <li>
                                <a href="#" >
                                    <i class="zmdi zmdi-settings"></i>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="text-custom">
                                    <i class="zmdi zmdi-power"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                            <li class="text-muted menu-title">Navigation</li>

                            <li>
                                <a href="<?= base_url('dashboard') ;?>" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
                            </li>

                            <li>
                                <a href="<?= base_url('users') ;?>" class="waves-effect"><i class="zmdi zmdi-accounts"></i> <span> Master Pengguna </span> </a>
                            </li>
                            <li>
                                <a href="<?= base_url('barang') ;?>" class="waves-effect"><i class="zmdi zmdi-folder-person"></i> <span> Master ATK </span> </a>
                            </li>                           
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-storage"></i> <span> ATK Management </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?= base_url('anggaran') ;?>">Penganggaran</a></li>
                                    <li><a href="<?= base_url('barangmasuk') ;?>">Barang Masuk</a></li>
                                    <li><a href="<?= base_url('barangkeluar') ;?>">Barang Keluar</a></li>
                                    <li><a href="<?= base_url('barangsisa') ;?>">Barang Sisa</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-balance"></i><span> Forecasting </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="chart-flot.html">Prediksi Penggunaan Triwulan</a></li>
                                    <li><a href="chart-morris.html">Optimasi Biaya</a></li>    
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->