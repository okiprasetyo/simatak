<!DOCTYPE html>
<html>
    <script id="tinyhippos-injected">
            if (window.top.ripple) { window.top.ripple("bootstrap").inject(window, document); }
        </script>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="<?php echo base_url("assetos/theme/assets/images/favicon.ico") ;?>">

        <title>Sistem Management ATK</title>
        

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="<?php echo base_url("assetos/theme/assets/plugins/morris/morris.css") ;?>">

        <!-- App css -->
        <link href="<?php echo base_url("assetos/theme/assets/css/bootstrap.min.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/core.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/components.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/icons.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/pages.css") ;?> ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/menu.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/responsive.css") ;?>" rel="stylesheet" type="text/css" />
        <!--calendar css-->
        <link href="<?php echo base_url("assetos/theme/assets/plugins/fullcalendar/dist/fullcalendar.css") ;?>" rel="stylesheet" />

        <!-- Alertifiti JS -->
        <link href="<?php echo base_url("assetos/theme/assets/plugins/alertifity/alertify.bootstrap.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/plugins/alertifity/alertify.core.css") ;?>" rel="stylesheet" type="text/css" />   
        <link href="<?php echo base_url("assetos/theme/assets/plugins/alertifity/alertify.default.css") ;?>" rel="stylesheet" type="text/css" />
        
       
        

         <!-- DataTables -->
        <link href="<?php echo base_url("assetos/theme/assets/plugins/datatables/jquery.dataTables.min.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/plugins/datatables/buttons.bootstrap.min.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/plugins/datatables/fixedHeader.bootstrap.min.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/plugins/datatables/responsive.bootstrap.min.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/plugins/datatables/scroller.bootstrap.min.css") ;?>" rel="stylesheet" type="text/css" />
        
          <!-- Plugins css-->
        <link href="<?php echo base_url("assetos/theme/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css") ;?>" rel="stylesheet" />
        <link href="<?php echo base_url("assetos/theme/assets/plugins/multiselect/css/multi-select.css") ;?>"  rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/plugins/select2/dist/css/select2.css") ;?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assetos/theme/assets/plugins/select2/dist/css/select2-bootstrap.css") ;?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assetos/theme/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css") ;?>" rel="stylesheet" />
        <link href="<?php echo base_url("assetos/theme/assets/plugins/switchery/switchery.min.css") ;?>" rel="stylesheet" />
        <link href="<?php echo base_url("assetos/theme/assets/plugins/timepicker/bootstrap-timepicker.min.css") ;?>" rel="stylesheet">
        <link href="<?php echo base_url("assetos/theme/assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css") ;?>" rel="stylesheet">
        <link href="<?php echo base_url("assetos/theme/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") ;?>" rel="stylesheet">
        <link href="<?php echo base_url("assetos/theme/assets/plugins/bootstrap-daterangepicker/daterangepicker.css") ;?>" rel="stylesheet">
        
         <script src="<?php echo base_url("assetos/theme/assets/js/jquery.min.js") ;?>"></script>

        <!-- Dashboard init -->
        <script src="<?php echo base_url("assetos/theme/assets/pages/jquery.dashboard.js");?>"></script>
        
        <!--Morris Chart-->
        <script src="<?php echo base_url("assetos/theme/assets/plugins/morris/morris.min.js") ;?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/plugins/raphael/raphael-min.js") ;?>"></script>


        
        


        <script>
            var resizefunc=[];
        </script>
    </head>
        <body class="fixed-left">
            <!-- Begin page -->
            <div id="wrapper">
                <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="#" class="logo"><span>Admin<span> Simatak</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title" align="center">Sistem Management ATK</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->