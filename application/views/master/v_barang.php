 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <p>
                                         <h1 class="header-title m-t-0 m-b-30">Master Data ATK</h1>
                                    </p>
                                    <div class="row"> 
                                    &nbsp; <button onclick="add_barang()" class="btn btn-success" data-toggle="modal" href='#modal-barang'>  <i class="glyphicon glyphicon-plus"></i>Tambah Data ATK </button> 
                                        <div class="col-sm-12">
                                        <div class="card-box table-responsive">                            
                                        <div class="col-sm-12">
                                        <div class="card-box table-responsive">
                                        <table id="table_barang" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Kode ATK</th>
                                                <th>Nama Barang ATK</th>
                                                <th>Satuan</th>
                                                <th>Harga Satuan (Rp.)</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        </table>
                                    </div>
                                     </div>
                                </div>
                            </div>
                            </div><!-- end row -->

                                </div>
                            </div><!-- end col -->
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->

                <footer class="footer">
                    2018 © Dev IT.
                </footer>
            </div>
            <!-- End content-page -->

<div class="modal fade" id="modal-barang">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Form ATK</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form" class="form-horizontal" accept-charset="utf-8">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Kode ATK* :</label>
                                <div class="col-sm-9">
                            <input type="text" name="kd_item" required="" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Nama ATK* :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="nama_item" class="form-control" >
                                </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Satuan :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="satuan" class="form-control" >
                                </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Harga Satuan :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="harga_satuan" class="form-control">
                                </div>
                        </div>
                    </div>
                
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="save()" id="btnSave">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
   
   var save_method;
   var table_barang;
    $(document).ready(function() {
        table_barang = $('#table_barang').dataTable({
            "ajax": '<?php echo base_url('barang/get_json')?>'
            });
    });

   function add_barang(){
    save_method = 'add';
      $('#form')[0].reset(); 
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
       $.ajax({
        url : "<?php echo base_url('barang/ajax_kode')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="kd_item"]').val(data.kode_item);
            
            $('#modal-barang').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Tambah User'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error Mengambil data');
        }
    });
   }
  
   function edit(id){
    save_method = 'update';
      $('#form')[0].reset();
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

      $.ajax({
        url : "<?php echo base_url('barang/ajax_edit')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="kd_item"]').val(data.kd_item);
            $('[name="nama_item"]').val(data.nama_item);
            $('[name="satuan"]').val(data.satuan);
            $('[name="harga_satuan"]').val(data.harga_satuan);
            
            $('#modal-barang').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error mengambil data');
        }
    });
   }

   function save(){
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo base_url('barang/ajax_add')?>";
        msg = "Berhasil menyimpan data"; 
    }else {
        url = "<?php echo base_url('barang/ajax_update')?>";
        msg = "Berhasil update data";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal-barang').modal('hide');
                alertify.success(msg);
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('Save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error Menyimpan / update data');
            $('#btnSave').text('Save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
   }

  function reload_table(){
            table_barang.api().ajax.reload(null,false); //reload datatable ajax 
    }

    function delete_barang(id){
      if(confirm('Apakah benar akan menghapus data ini?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "<?php echo base_url('barang/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               $('#modal-barang').modal('hide');
               alertify.success("Berhasil delete data");
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('error hapus data');
            }
        });
         
      }
  }


    
</script>



