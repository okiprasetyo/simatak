 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- <ul class="nav nav-tabs">
                                    <li role="presentation" class="active">
                                        <a href="<?= base_url("index.php/anggaran");?>" role="tab" data-toggle="tab">Triwulan I</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="<?= base_url("index.php/anggaran_tw2");?>" role="tab" data-toggle="tab">Triwulan II</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="<?= base_url('index.php/anggaran_tw3');?>" role="tab" data-toggle="tab">Triwulan III</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="<?= base_url('index.php/anggaran_tw4');?>" role="tab" data-toggle="tab">Triwulan IV</a>
                                    </li>
                                </ul> -->
                                <div class="card-box">
                                    <p>
                                         <h1 class="header-title m-t-0 m-b-30">Master Data Penganggaran</h1>
                                    </p>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="p-b-10">
                                                <p>
                                                    <button class="btn btn-primary waves-effect waves-light collapsed" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"> Tambah Data Penganggaran
                                                    </button>
                                                </p>
                                                <div class="collapse" id="collapseExample" aria-expanded="false" style="height: 0px;">
                                                    <div class="well"> 
                                                            <form class="form-horizontal" role="form" data-parsley-validate="" novalidate="">
                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">Pilih Kode ATK/Barang*</label>
                                                                 <div class="col-sm-7">
                                                                    <select class="form-control">
                                                                        <option>1</option>
                                                                        <option>2</option>
                                                                        <option>3</option>
                                                                        <option>4</option>
                                                                        <option>5</option>
                                                                    </select> 
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="hori-pass1" class="col-sm-4 control-label">Nama ATK/Barang</label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" id="hori-pass1" type="" placeholder="" required="" class="form-control" data-parsley-id="19">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">Pilih Triwulan*</label>
                                                                 <div class="col-sm-7">
                                                                    <select class="form-control">
                                                                        <option>Triwulan I</option>
                                                                        <option>Triwulan II</option>
                                                                        <option>Triwulan III</option>
                                                                        <option>Triwulan IV</option>
                                                                    </select> 
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="hori-pass3" class="col-sm-4 control-label">Jumlah ATK/Barang</label>
                                                                <div class="col-sm-7">
                                                                    <input data-parsley-equalto="#hori-pass3" type="password" required="" placeholder="" class="form-control" id="hori-pass3" data-parsley-id="21">
                                                                </div>
                                                            </div>
                                                             <div class="form-group">
                                                                <label for="hori-pass4" class="col-sm-4 control-label">Tanggal Penganggaran</label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-4 col-sm-8">
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                                        Simpan
                                                                    </button>
                                                                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                                                                        Batal
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div><!-- end col -->
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">  
                                                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                    <div class="row">
                                       <div class="col-sm-12">
                                            <div class="card-box table-responsive">
                                            <h4 class="header-title m-t-0 m-b-30">Cetak</h4>
                                            <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable-buttons_info">
                                            <thead>
                                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 79.0208px;">ID Anggaran</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 138.021px;">Kode ATK/Barang</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 56.0208px;">Nama Barang</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 26.0208px;">Triwulan</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 53.0208px;">Jumlah</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 47.0104px;">Sub Total</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 47.0104px;">Tanggal</th>
                                            </thead>
                                            <tbody>
                                                <tr role="row" class="odd">
                                                    <td class="sorting_1" tabindex="0">Airi Satou</td>
                                                    <td>Accountant</td>
                                                    <td>Tokyo</td>
                                                    <td>33</td>
                                                    <td>2008/11/28</td>
                                                    <td style="">$162,700</td>
                                                     <td style="">$162,700</td>
                                                </tr><tr role="row" class="even">
                                                    <td class="sorting_1" tabindex="0">Angelica Ramos</td>
                                                    <td>Chief Executive Officer (CEO)</td>
                                                    <td>London</td>
                                                    <td>47</td>
                                                    <td>2009/10/09</td>
                                                    <td style="">$1,200,000</td>
                                                    <td style="">$162,700</td>
                                                </tr><tr role="row" class="odd">
                                                    <td class="sorting_1" tabindex="0">Ashton Cox</td>
                                                    <td>Junior Technical Author</td>
                                                    <td>San Francisco</td>
                                                    <td>66</td>
                                                    <td>2009/01/12</td>
                                                    <td style="">$86,000</td>
                                                    <td style="">$162,700</td>
                                                </tr><tr role="row" class="even">
                                                    <td class="sorting_1" tabindex="0">Bradley Greer</td>
                                                    <td>Software Engineer</td>
                                                    <td>London</td>
                                                    <td>41</td>
                                                    <td>2012/10/13</td>
                                                    <td style="">$132,000</td>
                                                     <td style="">$162,700</td>
                                                </tr><tr role="row" class="odd">
                                                    <td class="sorting_1" tabindex="0">Brenden Wagner</td>
                                                    <td>Software Engineer</td>
                                                    <td>San Francisco</td>
                                                    <td>28</td>
                                                    <td>2011/06/07</td>
                                                    <td style="">$206,850</td>
                                                     <td style="">$162,700</td>
                                                </tr><tr role="row" class="even">
                                                    <td class="sorting_1" tabindex="0">Brielle Williamson</td>
                                                    <td>Integration Specialist</td>
                                                    <td>New York</td>
                                                    <td>61</td>
                                                    <td>2012/12/02</td>
                                                    <td style="">$372,000</td>
                                                    <td style="">$162,700</td>
                                                </tr><tr role="row" class="odd">
                                                    <td class="sorting_1" tabindex="0">Bruno Nash</td>
                                                    <td>Software Engineer</td>
                                                    <td>London</td>
                                                    <td>38</td>
                                                    <td>2011/05/03</td>
                                                    <td style="">$163,500</td>
                                                    <td style="">$162,700</td>
                                                </tr><tr role="row" class="even">
                                                    <td class="sorting_1" tabindex="0">Caesar Vance</td>
                                                    <td>Pre-Sales Support</td>
                                                    <td>New York</td>
                                                    <td>21</td>
                                                    <td>2011/12/12</td>
                                                    <td style="">$106,450</td>
                                                    <td style="">$162,700</td>
                                                </tr><tr role="row" class="odd">
                                                    <td class="sorting_1" tabindex="0">Cara Stevens</td>
                                                    <td>Sales Assistant</td>
                                                    <td>New York</td>
                                                    <td>46</td>
                                                    <td>2011/12/06</td>
                                                    <td style="">$145,600</td>
                                                     <td style="">$162,700</td>
                                                </tr><tr role="row" class="even">
                                                    <td class="sorting_1" tabindex="0">Cedric Kelly</td>
                                                    <td>Senior Javascript Developer</td>
                                                    <td>Edinburgh</td>
                                                    <td>22</td>
                                                    <td>2012/03/29</td>
                                                    <td style="">$433,060</td>
                                                     <td style="">$162,700</td>
                                                </tr></tbody>
                                        </table>

                                        <div class="dataTables_paginate paging_simple_numbers" id="datatable-buttons_paginate">
                                            
                                        </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                </div>
                                </div>
                            </div>
                            </div><!-- end row -->

                                </div>
                            </div><!-- end col -->
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->

                <footer class="footer">
                    2018 © Dev IT.
                </footer>
            </div>
            <!-- End content-page -->
         