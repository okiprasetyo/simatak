 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <p>
                                         <h1 class="header-title m-t-0 m-b-30">Data Barang Masuk</h1>
                                    </p>
                                    <div class="row"> 
                                    &nbsp; <button onclick="add_barang_masuk()" class="btn btn-success" data-toggle="modal" href='#modal-barang-masuk'><i class="glyphicon glyphicon-plus"></i> Tambah Data Barang Masuk </button> 
                                        <div class="col-sm-12">
                                        <div class="card-box table-responsive">                            
                                        <div class="col-sm-12">
                                        <div class="card-box table-responsive">
                                        <table id="table_barang_masuk" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Kode Barang Masuk</th>
                                                <th>Kode Barang </th>
                                                <th>Nama Barang</th>
                                                <th>Jumlah Barang Masuk</th>
                                                <th>Tanggal Masuk</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        </table>
                                    </div>
                                     </div>
                                </div>
                            </div>
                            </div><!-- end row -->

                                </div>
                            </div><!-- end col -->
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->

                <footer class="footer">
                    2018 © Dev IT.
                </footer>
            </div>
            <!-- End content-page -->

<div class="modal fade" id="modal-barang-masuk">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Form ATK</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form" class="form-horizontal" accept-charset="utf-8">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Kode ATK Masuk* :</label>
                            <div class="col-sm-9">
                                <input type="text" name="id_brg_masuk" required="" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Nama Barang* :</label>
                            <div class="col-sm-9">
                                <select name="kd_item" id="kd_item" class="form-control list-item">
                                   <option value="0"> -- Pilih Barang -- </option>

                               </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Jumlah :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="jumlah_brg_masuk" class="form-control">
                                </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Tanggal Masuk :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="tgl_brg_masuk" class="form-control">
                                </div>
                        </div>
                    </div>
                
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="save()" id="btnSave">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var save_method;
    var table_barang_masuk;

   $(document).ready(function() {
        table_barang_masuk_= $('#table_barang_masuk').dataTable({
                             "ajax": '<?php echo base_url('Barangmasuk/get_json')?>'
                         });
       
    });
     function add_barang_masuk(){
        save_method = 'add';
          $('#form')[0].reset(); 
          $('.form-group').removeClass('has-error'); // clear error class
          $('.help-block').empty(); // clear error string
           $.ajax({
            url : "<?php echo base_url('Barangmasuk/ajax_kode')?>",
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id_brg_masuk"]').val(data.kd_brg_masuk);

                var listitem = '';
                $.each(data.kd_brg, function(index, val) {
                     /* iterate through array or object */
                     listitem += '<option value="'+val.kd_item+'">'+val.nama_item+'</option>'
                });

                $('.list-item').append(listitem);
                $('#modal-barang-masuk').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Tambah Barang Masuk'); // Set title to Bootstrap modal title
                
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Mengambil data');
            }
        });
     }

     function edit(id){
        save_method = "update";
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        $.ajax({
        url : "<?php echo base_url('Barangmasuk/ajax_edit')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_brg_masuk"]').val(data.id_brg_masuk);
            $('[name="kd_item"]').val(data.kd_item);
            $('[name="jumlah_brg_masuk"]').val(data.jumlah_brg_masuk);
            $('[name="tgl_brg_masuk"]').val(data.tanggal_brg_masuk);
           
            $('#modal-barang-masuk').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error mengambil data');
        }
      });
    }

    function save(){
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo base_url('Barangmasuk/ajax_add')?>";
        msg = "Berhasil menyimpan data"; 
    }else {
        url = "<?php echo base_url('Barangmasuk/ajax_update')?>";
        msg = "Berhasil update data";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal-barang-masuk').modal('hide');
               
                alertify.success(msg);
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('Save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error Menyimpan / update data');
            $('#btnSave').text('Save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
   }


    
</script>



