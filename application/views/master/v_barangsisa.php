 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <p>
                                         <h1 class="header-title m-t-0 m-b-30">Barang Sisa</h1>
                                    </p>
                                   
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="p-b-10">
                                                <p>
                                                    <!-- <button class="btn btn-primary waves-effect waves-light collapsed" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"> Tambah Data Barang Keluar
                                                    </button> -->
                                                </p>
                                                <div class="collapse" id="collapseExample" aria-expanded="false" style="height: 0px;">
                                                    <div class="well"> 
                                                            <form class="form-horizontal" role="form" data-parsley-validate="" novalidate="">
                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">Kode ATK/Barang*</label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" required="" parsley-type="email" class="form-control" id="inputEmail3" placeholder="" data-parsley-id="17">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="hori-pass1" class="col-sm-4 control-label">Nama Lengkap</label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" id="hori-pass1" type="" placeholder="" required="" class="form-control" data-parsley-id="19">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="hori-pass1" class="col-sm-4 control-label">E-Mail</label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" id="hori-pass1" type="" placeholder="" required="" class="form-control" data-parsley-id="19">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="hori-pass2" class="col-sm-4 control-label">Jabatan</label>
                                                                <div class="col-sm-7">
                                                                    <input data-parsley-equalto="#hori-pass1" type="password" required="" placeholder="" class="form-control" id="hori-pass2" data-parsley-id="21">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="webSite" class="col-sm-4 control-label">Passwords</label>
                                                                <div class="col-sm-7">
                                                                    <input type="password" required="" class="form-control" id="" placeholder="" data-parsley-id="23">
                                                                </div>
                                                            </div>
                                                          
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-4 col-sm-8">
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                                        Simpan
                                                                    </button>
                                                                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                                                                        Batal
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div><!-- end col -->
                                        <div class="col-sm-12">
                                <div class="card-box table-responsive"> 
                                <h6 class="header-title m-t-0 m-b-30">Data Barang Sisa</h6>
                                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                   <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <h4 class="header-title m-t-0 m-b-30">Pilih Cetak</h4>
                                    <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                       
                                        <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable-buttons_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 79.0208px;">Kode ATK/Barang</th>
                                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 138.021px;">Nama Barang</th>
                                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 56.0208px;">Jumlah Sisa</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr role="row" class="odd">
                                                <td class="sorting_1" tabindex="0">Airi Satou</td>
                                                <td>Accountant</td>
                                                <td>Tokyo</td>
                                                
                                            </tr><tr role="row" class="even">
                                                <td class="sorting_1" tabindex="0">Angelica Ramos</td>
                                                <td>Chief Executive Officer (CEO)</td>
                                                <td>London</td>
                                                
                                            </tr><tr role="row" class="odd">
                                                <td class="sorting_1" tabindex="0">Ashton Cox</td>
                                                <td>Junior Technical Author</td>
                                                <td>San Francisco</td>
                                                
                                            </tr><tr role="row" class="even">
                                                <td class="sorting_1" tabindex="0">Bradley Greer</td>
                                                <td>Software Engineer</td>
                                                <td>London</td>
                                                
                                            </tr><tr role="row" class="odd">
                                                <td class="sorting_1" tabindex="0">Brenden Wagner</td>
                                                <td>Software Engineer</td>
                                                <td>San Francisco</td>
                                                
                                            </tr><tr role="row" class="even">
                                                <td class="sorting_1" tabindex="0">Brielle Williamson</td>
                                                <td>Integration Specialist</td>
                                                <td>New York</td>
                                                
                                            </tr><tr role="row" class="odd">
                                                <td class="sorting_1" tabindex="0">Bruno Nash</td>
                                                <td>Software Engineer</td>
                                                <td>London</td>
                                                
                                            </tr><tr role="row" class="even">
                                                <td class="sorting_1" tabindex="0">Caesar Vance</td>
                                                <td>Pre-Sales Support</td>
                                                <td>New York</td>
                                                
                                            </tr><tr role="row" class="odd">
                                                <td class="sorting_1" tabindex="0">Cara Stevens</td>
                                                <td>Sales Assistant</td>
                                                <td>New York</td>
                                                
                                            </tr><tr role="row" class="even">
                                                <td class="sorting_1" tabindex="0">Cedric Kelly</td>
                                                <td>Senior Javascript Developer</td>
                                                <td>Edinburgh</td>
                                                
                                            </tr></tbody>
                                    </table>

                                    <div class="dataTables_paginate paging_simple_numbers" id="datatable-buttons_paginate">
                                        
                                    </div>
                                </div>
                                </div>
                            </div>
                                </div>
                            
                             
                            </div>
                                </div>
                            </div>
                            </div><!-- end row -->

                                </div>
                            </div><!-- end col -->
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->

                <footer class="footer">
                    2018 © Dev IT.
                </footer>
            </div>
            <!-- End content-page -->
         