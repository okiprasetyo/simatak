 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <p>
                                         <h1 class="header-title m-t-0 m-b-30">Master Data User</h1>
                                    </p>
                                   
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="p-b-10">
                                                <button onclick="add_users()" class="btn btn-success" data-toggle="modal" href='#modal-user'>  <i class="glyphicon glyphicon-plus"></i>Tambah Data User  
                                                </button>
                                            </div>
                                        </div><!-- end col -->
                                        <div class="col-sm-12">
                                <div class="card-box table-responsive">                            
                                    <div class="col-sm-12">
                                       
                                        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>NIP</th>
                                                <th>Nama</th>
                                                <th>Jabatan</th>
                                                <th>E-Mail</th>
                                                <th>Passwords</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        </table>
                                 
                                     </div>
                                </div>
                            </div>
                            </div><!-- end row -->

                                </div>
                            </div><!-- end col -->
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->

                <footer class="footer">
                    2018 © Dev IT.
                </footer>
            </div>
            <!-- End content-page -->

<div class="modal fade" id="modal-user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Form User</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form" class="form-horizontal" accept-charset="utf-8">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">NIP * :</label>
                                <div class="col-sm-9">
                            <input type="text" name="nip" required="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Nama * :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="nama" class="form-control" >
                                </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3"> Jabatan :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="jabatan" class="form-control" >
                                </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">E-Mail :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="email" class="form-control">
                                </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Password:</label>
                                <div class="col-sm-9">
                                    <input type="text" name="passwords" class="form-control">
                                </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="save()" id="btnSave">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  
   var save_method;
   var table;
   
   $(document).ready(function() {
        table= $('#table').dataTable({
                             "ajax": '<?php echo base_url('Users/get_json')?>'
                         });
       
    });

   function add_users(){
    save_method = 'add';

      $('#form')[0].reset(); 
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
       $.ajax({
        url : "<?php echo base_url('Users/ajax_kode')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="nip"]').val(data.kode_user);
            
            $('#modal-user').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Tambah User'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error Mengambil data');
        }
    });
   }

   function edit(id){
    save_method = 'update';
      $('#form')[0].reset();
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

      $.ajax({
        url : "<?php echo base_url('Users/ajax_edit')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="nip"]').val(data.nip);
            $('[name="nama"]').val(data.nama);
            $('[name="jabatan"]').val(data.jabatan);
            $('[name="email"]').val(data.email);
            $('[name="passwords"]').val(data.passwords);
            $('#modal-user').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error mengambil data');
        }
    });
   }

   function save(){
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo base_url('Users/ajax_add')?>";
        msg = "Berhasil menyimpan data"; 
    }else {
        url = "<?php echo base_url('Users/ajax_update')?>";
        msg = "Berhasil update data";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal-user').modal('hide');
               
                alertify.success(msg);
                reload_table_user();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('Save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error Menyimpan / update data');
            $('#btnSave').text('Save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
   }


    function reload_table_user(){
       table.api().ajax.reload();
   }

    function delete_user(id){
      if(confirm('Apakah benar akan menghapus data ini ?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "<?php echo base_url('Users/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               $('#modal-user').modal('hide');
               alertify.success("Berhasil delete data");
               reload_table_user();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('error hapus data');
            }
        });
         
      }
  }

  
   
</script>



