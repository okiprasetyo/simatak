<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title>Login</title>

        <!-- App CSS -->
        <link href="<?php echo base_url("assetos/theme/assets/css/bootstrap.min.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/core.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/components.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/icons.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/pages.css") ;?> ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/menu.css") ;?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assetos/theme/assets/css/responsive.css") ;?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url("assetos/theme/assets/js/modernizr.min.js") ;?>"></script>
        
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="text-center">
                <a href="#" class="logo"><span>Admin<span>Sistem Management ATK</span></span></a>
                <h3>SIMATAK Pajak Daerah I</h3>
            </div>
            <div class="m-t-40 card-box">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0">Sign In</h4>
                     <?php 
                        echo validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">×</button>','</div>');
                        echo form_open('login/validate'); 
                    ?>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal m-t-20" action="">

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input name="nip" class="form-control" type="text" required="" placeholder="Nomor Induk Pegawai" value="<?php echo set_value('nip'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <input name="passwords" class="form-control" type="password" required="" placeholder="Password" value="<?php echo set_value('passwords'); ?>">
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <div class="checkbox checkbox-custom">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup">
                                        Remember me
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group text-center m-t-30">
                            <div class="col-xs-12">
                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>
                    <?php echo form_close();?>
                        <div class="form-group m-t-30 m-b-0">
                            <div class="col-sm-12">
                                <a href="page-recoverpw.html" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- end card-box-->

            <div class="row">
                <div class="col-sm-12 text-center">
                    <p class="text-muted">Don't have an account? <a href="page-register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
                </div>
            </div>
            
        </div>
        <!-- end wrapper page -->
        

        
        <script>
            var resizefunc = [];
        </script>
        <script>
          $(function () {
            $('input').iCheck({
              checkboxClass: 'icheckbox_square-blue',
              radioClass: 'iradio_square-blue',
              increaseArea: '20%' // optional
            });
          });
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url("assetos/theme/assets/js/jquery.min.js") ;?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/js/bootstrap.min.js");?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/js/detect.js");?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/js/fastclick.js");?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/js/jquery.slimscroll.js");?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/js/jquery.blockUI.js");?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/js/waves.js");?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/js/wow.min.js");?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/js/jquery.nicescroll.js");?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/js/jquery.scrollTo.min.js");?>"></script>

        <!-- App js -->
        <script src="<?php echo base_url("assetos/theme/assets/js/jquery.core.js") ;?>"></script>
        <script src="<?php echo base_url("assetos/theme/assets/js/jquery.app.js");?>"></script>
    
    </body>
</html>