<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_users extends CI_Model {

	var $table = 't_users';

	public function get_datatables()
	{
		 $query = $this->db->get($this->table);
    	 return $query->result();
	}

	public function get_by_id($id){
		$this->db->from('t_users');
		$this->db->where('nip', $id);
		$query = $this->db->get();
		return $query->row();
	}	
	
	public function save($data){
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id){
		$this->db->where('nip', $id);
		$this->db->delete($this->table);
	}

}

/* End of file M_users.php */
/* Location: ./application/models/M_users.php */