<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang extends CI_Model {

	var $table = 't_master_brg';


	public function get_datatables(){
		$query = $this->db->get($this->table);
    	return $query->result();
	}

	public function get_by_id($id){
		$this->db->from('t_master_brg');
		$this->db->where('kd_item', $id);
		$query = $this->db->get(); 
		return $query->row();
	}

	public function getMaxKodeBarang(){
		$q = $this->db->query("select MAX(RIGHT(kd_item,4)) as kd_max from t_master_brg");
		$kd = "";
		if($q->num_rows()>0)
		{
			foreach($q->result() as $k)
			{
				$tmp = ((int)$k->kd_max)+1;
				$kd = sprintf("%04s", $tmp);
			}
		}
		else
		{
			$kd = "0001";
		}	
		return "BRG-".$kd;
	}

	public function save($data){
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('kd_item', $id);
		$this->db->delete($this->table);
	}

}

/* End of file M_barang.php */
/* Location: ./application/models/M_barang.php */