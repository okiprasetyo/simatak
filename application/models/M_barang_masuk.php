<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang_masuk extends CI_Model {

	var $table = 't_brg_masuk';

	public function get_kd_brg(){
		$this->db->select('kd_item, nama_item');
		$query = $this->db->get('t_master_brg');
		return $query->result();
	}

	public function get_barang_masuk(){

		$this->db->select('a.id_brg_masuk, a.kd_item, b.nama_item, a.jumlah_brg_masuk, a.tanggal_brg_masuk');
		$this->db->join('t_master_brg b','a.kd_item=b.kd_item','left');
		$query = $this->db->get('t_brg_masuk a');
		return $query->result();
	}

	public function get_nama_item($id){
		$this->db->select('nama_item from t_master_brg');
		$this->db->where('kd_item',$id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function get_by_id($id){
		$this->db->from('t_brg_masuk');
		$this->db->where('id_brg_masuk', $id);
		$query = $this->db->get(); 
		return $query->row();
	}

	public function getMaxKodeBrgMasuk(){
		$q = $this->db->query("select MAX(RIGHT(id_brg_masuk,4)) as kd_max from t_brg_masuk");
		$kd = "";
		if($q->num_rows()>0)
		{
			foreach($q->result() as $k)
			{
				$tmp = ((int)$k->kd_max)+1;
				$kd = sprintf("%04s", $tmp);
			}
		}
		else
		{
			$kd = "0001";
		}	
		return "IN".$kd;
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}
	

}

/* End of file M_barang_masuk.php */
/* Location: ./application/models/M_barang_masuk.php */